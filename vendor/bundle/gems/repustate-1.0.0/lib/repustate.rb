#
# Repustate Ruby API client.
#
# Requirements:
#  - Ruby >= 1.8.7
#  - json
#
# Want to change it / improve it / share it? Go for it.
#
# Feedback is appreciated at info@repustate.com
#
# More documentation available at http://www.repustate.com/docs
#

require 'cgi'
require 'net/http'

require 'rubygems'
require 'json'

class Repustate
  def initialize(key, version='v3')
    @key = key
    @version = version
  end

  # Retrieve the sentiment for a single URl or block of text. Optionally select
  # a language other than English.
  def sentiment(options={:text => nil, :lang => 'en'})
    call_api('score', options)
  end

  # Bulk score multiple pieces of text (not urls!).
  def bulk_sentiment(options={:items => [], :lang => 'en'})
    items_to_score = Hash[options[:items].enum_for(:each_with_index).collect {
                            |val, i| ["text#{i}", val]
                          }]
    call_api('bulk-score', items_to_score)
  end

  # Clean up a web page. It doesn't work well on home pages - it's
  # designed for content pages.
  def clean_html(options={:url => nil})
    use_http_get = true
    call_api('clean-html', options, use_http_get)
  end
  
  def entities(options={:text => nil, :lang => 'en'})
    use_http_get = true
    call_api('entities', options)
  end
  
  def topic(options={:text => nil, :topics => nil, :lang => 'en'})
    use_http_get = true
    call_api('entities', options)
  end
  
  protected

  def url_for_call(api_function)
    return "http://api.repustate.com/v3/ccca9abb9974995ce4447bf48bd2e1c9bae695ea/#{api_function}.json"
  end

  def get_http_result(url, args, use_http_get)
    if use_http_get
      query = args.collect { |k,v| "#{k}=#{CGI::escape(v.to_s)}" }.join('&')
      url = url.concat("?#{query}")
      result = Net::HTTP.get_response(URI.parse(url))
    else
      result = Net::HTTP.post_form(URI.parse(url), args)
    end

    if result.code != '200'
      raise "HTTP Error: #{result.code} #{result.body}"
    end

    result.body
  end

  def parse_result(result, api_function)
    return JSON.parse(result)
  end

  def call_api(api_function, args={}, use_http_get=false)
    # Avoid sending empty parameters
    args = args.select { |key, value| value and not value.empty? }

    url = url_for_call(api_function)
    result = get_http_result(url, args, use_http_get)
    parse_result(result, api_function)
  end

end
