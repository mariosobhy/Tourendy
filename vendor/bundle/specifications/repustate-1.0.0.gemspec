# -*- encoding: utf-8 -*-
# stub: repustate 1.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "repustate".freeze
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Repustate".freeze]
  s.date = "2015-04-30"
  s.description = "A client library to the Repustate text analytics API".freeze
  s.email = "info@repustate.com".freeze
  s.homepage = "https://www.repustate.com".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.6.12".freeze
  s.summary = "Repustate text analytics".freeze

  s.installed_by_version = "2.6.12" if s.respond_to? :installed_by_version
end
