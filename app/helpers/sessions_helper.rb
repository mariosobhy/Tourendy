module SessionsHelper
  def log_in(user)
    # session used to save temporary data in web browser until it's close
    session[:user_id] = user.id
  end

  # returning the current user logged-in
  def current_user
    #if @current_user.nil?
      #User.find_by(id: session[:user_id])
    #else
      #@current_user
    #end
    #@current_user = @current_user || User.find_by(id: session[:user_id])
    @current_user ||= User.find_by(id: session[:user_id])

  end

  def logged_in?
   !current_user.nil?
  end

  # Logs out the current user.
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
end
