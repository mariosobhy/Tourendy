
require 'repustate'

class SentimentAnalysis
	def initialize
		@client = Repustate.new('ccca9abb9974995ce4447bf48bd2e1c9bae695ea')
	end
	def sentiments_statuses(sentiments)
		#ToDo refactoring
		@sentiments_statuses = {positive: 0, negative: 0, neutral: 0}
		sentiments.each do |sent|
			if sentiment_score(sent) > 0
				@sentiments_statuses[:positive] += 1
			elsif sentiment_score(sent) < 0 
				@sentiments_statuses[:negative] += 1
			else
				@sentiments_statuses[:neutral] += 1
			end
		end
		@sentiments_statuses
	end
	def sentiment_score(sent)
		@client.sentiment(text: sent).to_a[0][1]
	end
end



