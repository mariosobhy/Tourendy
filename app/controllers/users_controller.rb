class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def show
    @user = User.find(params[:id])
  end
  #def create
    #params[:user] is hash of user attributes
    #params[:user] -->  "name" => "Foo Bar", "email" => "foo@invalid", "password" => "[FILTERED]", "password_confirmation" => "[FILTERED]"
   # @user = User.new(params[:user])     # Not the final implementation!
    #if @user.save
      # Handle a successful save.
    #else
     # render 'new'
    #end
  #end
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      # Handle a successful save.
      flash[:success] = "Welcome to Tourendy"
      redirect_to user_url(@user)
    else
      # when submitting the invalid data
      render 'new'
    end
  end

  private
  # to avoid any user putting any data except those
  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end
end
