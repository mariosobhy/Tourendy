 class SessionsController < ApplicationController
  def new
  end

  def create
   # fuser = FUser.from_omniauth(env['omniauth.auth'])
   # session[:fuser_id] = fuser.id
   # redirect_to fuser_url(fuser)

    user = User.find_by(email: params[:session][:email].downcase)
    #user in if condition refers to check user is valid (not nil)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page
      log_in user
      redirect_to user_url(user)
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    #sessions[:user_id] = nil
    #redirect_to root_url

    log_out
    redirect_to root_url
x   end
end

