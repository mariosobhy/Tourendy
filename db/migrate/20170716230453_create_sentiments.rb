class CreateSentiments < ActiveRecord::Migration[5.0]
  def change
    create_table :sentiments do |t|
      t.string :author_name
      t.string :author_url
      t.string :language
      t.integer :rating
      t.string :relative_time_description
      t.string :text

      t.timestamps
    end
  end
end
