Rails.application.routes.draw do
  ActiveAdmin.routes(self)
#  post 'auth/:provider/callback', to: 'sessions#create'
 # post 'auth/failure', to: redirect('/')
  #delete 'signout', to: 'sessions#destroy', as: 'signout'

  get 'sessions/new'

  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get  '/login',   to: 'sessions#new'
  post '/login',   to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users
end